/**
 * Task 1.
 */

package com.axeane.model;

@FunctionalInterface
public interface FuncInterface {
  int getFunction(int a, int b, int c);
}
