/**
 * Task 2.
 */

package com.axeane.model;

public interface Command {
  String execute(String str);
}
