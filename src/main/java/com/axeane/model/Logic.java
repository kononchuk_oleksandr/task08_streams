package com.axeane.model;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Logic {
  private static Logger logger = LogManager.getLogger(Logic.class);

  //======================= TASK 1 =======================
  public int getMaxNumber(int a, int b, int c) {
    FuncInterface funcInterface =
        (one, two, three) -> Math.max(Math.max(three, two), one);
    return funcInterface.getFunction(a, b, c);
  }

  public int getAverageNumber(int a, int b, int c) {
    FuncInterface funcInterface = (one, two, three) -> (one + two + three) / 3;
    return funcInterface.getFunction(a, b, c);
  }

  //======================= TASK 2 =======================
  public String toUpperCaseCommand(String text) {
    Command command = str -> str.toUpperCase();
    return command.execute(text);
  }

  public String toLowerCaseCommand(String text) {
    Command command = String::toLowerCase;
    return command.execute(text);
  }

  public String replaceAllCommand(String text) {
    Command command = new Command() {
      @Override
      public String execute(String str) {
        return str.replaceAll("a", "*");
      }
    };
    return command.execute(text);
  }

  public String getLengthCommand(String text) {
    return new GetLengthCommand().execute(text);
  }

  //======================= TASK 3 =======================
  public List<Integer> list() {
    List<Integer> list = new ArrayList<>();
    Random random = new Random();
    for (int i = 0; i < 5; i++) {
      list.add(random.nextInt());
    }
    return list;
  }

  public String averageStream() {
    List<Integer> list = list();
    logger.info("List: " + String.join(", ", list.toString()));
    return list.stream()
        .mapToInt(Integer::intValue)
        .average()
        .toString();
  }

  public String minStream() {
    List<Integer> list = list();
    logger.info("List: " + String.join(", ", list.toString()));
    return list.stream()
        .mapToInt(Integer::intValue)
        .min()
        .toString();
  }

  public String maxStream() {
    List<Integer> list = list();
    logger.info("List: " + String.join(", ", list.toString()));
    return list.stream()
        .mapToInt(Integer::intValue)
        .max()
        .toString();
  }

  public Long sumStream() {
    List<Integer> list = list();
    logger.info("List: " + String.join(", ", list.toString()));
    return list.stream()
        .mapToLong(Integer::intValue)
        .sum();
  }

  public String sumReduceStream() {
    List<Integer> list = list();
    logger.info("List: " + String.join(", ", list.toString()));
    return list.stream()
        .reduce(Integer::sum)
        .toString();
  }

  public Long biggerThenAverStream() {
    List<Integer> list = list();
    logger.info("List: " + String.join(", ", list.toString()));

    IntSummaryStatistics statistics = list.stream()
        .mapToInt(Integer::intValue)
        .summaryStatistics();

    return list.stream()
        .mapToInt(Integer::intValue)
        .filter(a -> a > statistics.getAverage())
        .count();
  }
}
