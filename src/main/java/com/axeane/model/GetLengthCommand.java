/**
 * Task 2.
 */

package com.axeane.model;

public class GetLengthCommand implements Command {

  @Override
  public String execute(String str) {
    return "The length of text is: " + str.length();
  }
}
