package com.axeane.view;

import com.axeane.controller.Controller;
import com.axeane.controller.ControllerImpl;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TaskThreeView {
  private static Logger logger = LogManager.getLogger(TaskThreeView.class);
  private Controller controller = new ControllerImpl();
  private Scanner scanner = new Scanner(System.in);
  private Map<String, String> taskThreeMenu;
  private Map<String, Printable> taskThreeMethods;

  public TaskThreeView() {
    taskThreeMenu = new LinkedHashMap<>();
    taskThreeMenu.put("1", "1 - Get average number from list");
    taskThreeMenu.put("2", "2 - Get minimum number from list");
    taskThreeMenu.put("3", "3 - Get maximum number from list");
    taskThreeMenu.put("4", "4 - Get sum of numbers from list");
    taskThreeMenu.put("5", "5 - Get sum of numbers (reduce) from list");
    taskThreeMenu.put("6", "6 - Get numbers bigger then average from list");
    taskThreeMenu.put("E", "E - Exit");

    taskThreeMethods = new LinkedHashMap<>();
    taskThreeMethods.put("1", this::averageStream);
    taskThreeMethods.put("2", this::minStream);
    taskThreeMethods.put("3", this::maxStream);
    taskThreeMethods.put("4", this::sumStream);
    taskThreeMethods.put("5", this::sumReduceStream);
    taskThreeMethods.put("6", this::biggerThenAverStream);
  }

  private void averageStream() {
    logger.info("Average number is: " +
        controller.averageStream());
    throw new BackException();
  }

  private void minStream() {
    logger.info("Minimum number is: " +
        controller.minStream());
    throw new BackException();
  }

  private void maxStream() {
    logger.info("Maximum number is: " +
        controller.maxStream());
    throw new BackException();
  }

  private void sumStream() {
    logger.info("Sum of numbers is: " +
        controller.sumStream());
    throw new BackException();
  }

  private void sumReduceStream() {
    logger.info("Sum of numbers is: " +
        controller.sumReduceStream());
    throw new BackException();
  }
  private void biggerThenAverStream() {
    logger.info("Count of numbers bigger then average is: " +
        controller.biggerThenAverStream());
    throw new BackException();
  }

  public void taskThreeMenu() {
    String key;

    taskThreeMenu.values().forEach(str -> logger.info(str));

    while (true) {
      key = scanner.nextLine();
      if (key.equalsIgnoreCase("e")) {
        break;
      } else {
        taskThreeMethods.get(key).print();
      }
    }
  }
}
