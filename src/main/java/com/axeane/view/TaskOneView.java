package com.axeane.view;

import com.axeane.controller.Controller;
import com.axeane.controller.ControllerImpl;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TaskOneView {
  private static Logger logger = LogManager.getLogger(TaskOneView.class);
  private Controller controller = new ControllerImpl();
  private Scanner scanner = new Scanner(System.in);
  private Map<String, String> taskOneMenu;
  private Map<String, Printable> taskOneMethods;

  public TaskOneView() {
    taskOneMenu = new LinkedHashMap<>();
    taskOneMenu.put("1", "1 - Get maximum number");
    taskOneMenu.put("2", "2 - Get average number");
    taskOneMenu.put("E", "E - Exit");

    taskOneMethods = new LinkedHashMap<>();
    taskOneMethods.put("1", this::getMaxNumber);
    taskOneMethods.put("2", this::getAverageNumber);
  }

  private void getMaxNumber() {
    logger.info("Function \"Maximum number\". Please write 3 numbers (split by enter):");
    int max = controller.getMaxNumber(scanner.nextInt(),
        scanner.nextInt(), scanner.nextInt());
    logger.info("The maximum number is: " + max);
  }

  private void getAverageNumber() {
    logger.info("Function \"Average Number\". Please write 3 numbers (split by enter):");
    int average = controller.getAverageNumber(scanner.nextInt(),
        scanner.nextInt(), scanner.nextInt());
    logger.info("Average number is: " + average);
  }

  public void taskOneMenu() {
    String key;

    taskOneMenu.values().forEach(str -> logger.info(str));

    while (true) {
      key = scanner.nextLine();
      if (key.equalsIgnoreCase("e")) {
        break;
      } else {
        taskOneMethods.get(key).print();
      }
    }
  }
}
