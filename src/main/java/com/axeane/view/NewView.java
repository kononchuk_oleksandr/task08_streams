package com.axeane.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.*;

public class NewView {
  private static Logger logger = LogManager.getLogger(NewView.class);
  private Scanner scanner = new Scanner(System.in);
  private Map<String, String> menu;
  private Map<String, Printable> menuMethods;
  private TaskOneView taskOneView;
  private TaskTwoView taskTwoView;
  private TaskThreeView taskThreeView;

  public NewView() {
    taskOneView = new TaskOneView();
    taskTwoView = new TaskTwoView();
    taskThreeView = new TaskThreeView();

    menu = new LinkedHashMap<>();
    menu.put("1", "1 - Task 1");
    menu.put("2", "2 - Task 2");
    menu.put("3", "3 - Task 3");
    menu.put("4", "4 - Task 4");
    menu.put("E", "E - Exit");

    menuMethods = new LinkedHashMap<>();
    menuMethods.put("1", taskOneView::taskOneMenu);
    menuMethods.put("2", taskTwoView::taskTwoMenu);
    menuMethods.put("3", taskThreeView::taskThreeMenu);
  }

  private void menu() {
    menu.values().forEach(str -> logger.info(str));
  }

  public void start() {
    String key = null;
    do {
      menu();
      logger.info("Push the button:");
      key = scanner.nextLine();
      try {
        menuMethods.get(key).print();
      } catch (Exception e) {
        e.getMessage();
      }
    } while (!key.equalsIgnoreCase("e"));
  }
}
