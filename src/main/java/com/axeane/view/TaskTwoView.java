package com.axeane.view;

import com.axeane.controller.Controller;
import com.axeane.controller.ControllerImpl;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TaskTwoView {
  private static Logger logger = LogManager.getLogger(TaskTwoView.class);
  private Controller controller = new ControllerImpl();
  private Scanner scanner = new Scanner(System.in);
  private Map<String, String> taskTwoMenu;
  private Map<String, Printable> taskTwoMethods;

  public TaskTwoView() {
    taskTwoMenu = new LinkedHashMap<>();
    taskTwoMenu.put("1", "1 - Command \"to upper case\"");
    taskTwoMenu.put("2", "2 - Command \"to lower case\"");
    taskTwoMenu.put("3", "3 - Command \"replace all\"");
    taskTwoMenu.put("4", "4 - Command \"get length\"");
    taskTwoMenu.put("E", "E - Exit");

    taskTwoMethods = new LinkedHashMap<>();
    taskTwoMethods.put("1", this::toUpperCaseCommand);
    taskTwoMethods.put("2", this::toLowerCaseCommand);
    taskTwoMethods.put("3", this::replaceAllCommand);
    taskTwoMethods.put("4", this::getLengthCommand);
  }

  private void toUpperCaseCommand() {
    logger.info("Please enter some text: ");
    String text = controller.toUpperCaseCommand(scanner.nextLine());
    logger.info("New text is: " + text);
    throw new BackException();
  }
  private void toLowerCaseCommand() {
    logger.info("Please enter some text: ");
    String text = controller.toLowerCaseCommand(scanner.nextLine());
    logger.info("New text is: " + text);
    throw new BackException();
  }
  private void replaceAllCommand() {
    logger.info("I'll replace all your letters \"a\" to \"*\"\n"
        + "Please enter some text: ");
    String text = controller.replaceAllCommand(scanner.nextLine());
    logger.info("New text is: " + text);
    throw new BackException();
  }
  private void getLengthCommand() {
    logger.info("Please enter some text: ");
    String text = controller.getLengthCommand(scanner.nextLine());
    logger.info("Length of text is: " + text);
    throw new BackException();
  }

  public void taskTwoMenu() {
    String key;

    taskTwoMenu.values().forEach(str -> logger.info(str));

    while (true) {
      key = scanner.nextLine();
      if (key.equalsIgnoreCase("e")) {
        break;
      } else {
        taskTwoMethods.get(key).print();
      }
    }
  }
}
