package com.axeane.view;

public interface Printable {
  void print();
}
