package com.axeane.controller;

public interface Controller {
  //TASK 1
  int getMaxNumber(int a, int b, int c);
  int getAverageNumber(int a, int b, int c);

  //TASK 2
  String toUpperCaseCommand(String text);
  String toLowerCaseCommand(String text);
  String replaceAllCommand(String text);
  String getLengthCommand(String text);

  //TASK 3
  String averageStream();
  String minStream();
  String maxStream();
  Long sumStream();
  String sumReduceStream();
  Long biggerThenAverStream();
}
