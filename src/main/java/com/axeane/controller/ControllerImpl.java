package com.axeane.controller;

import com.axeane.model.Logic;

public class ControllerImpl implements Controller {
  Logic logic;

  public ControllerImpl() {
    logic = new Logic();
  }

  @Override
  public int getMaxNumber(int a, int b, int c) {
    return logic.getMaxNumber(a, b, c);
  }

  @Override
  public int getAverageNumber(int a, int b, int c) {
    return logic.getAverageNumber(a, b, c);
  }

  @Override
  public String toUpperCaseCommand(String text) {
    return logic.toUpperCaseCommand(text);
  }

  @Override
  public String toLowerCaseCommand(String text) {
    return logic.toLowerCaseCommand(text);
  }

  @Override
  public String replaceAllCommand(String text) {
    return logic.replaceAllCommand(text);
  }

  @Override
  public String getLengthCommand(String text) {
    return logic.getLengthCommand(text);
  }

  @Override
  public String averageStream() {
    return logic.averageStream();
  }

  @Override
  public String minStream() {
    return logic.minStream();
  }

  @Override
  public String maxStream() {
    return logic.maxStream();
  }

  @Override
  public Long sumStream() {
    return logic.sumStream();
  }

  @Override
  public String sumReduceStream() {
    return logic.sumReduceStream();
  }

  @Override
  public Long biggerThenAverStream() {
    return logic.biggerThenAverStream();
  }
}